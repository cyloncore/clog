/**
 * logging file allowing optional support of clog
 */

#include <%PROJECT%/config.h>

#ifdef %PROJECT_UC%_HAVE_CLOG

#include <clog>

#define %PROJECT%_error(...) clog_error(__VA_ARGS__)
#define %PROJECT%_warning(...) clog_warning(__VA_ARGS__)
#define %PROJECT%_debug(...) clog_debug(__VA_ARGS__)
#define %PROJECT%_info(...) clog_info(__VA_ARGS__)
#define %PROJECT%_fatal(...) clog_fatal(__VA_ARGS__)

#define %PROJECT%_debug_vn(...) clog_debug_vn(__VA_ARGS__)

#define %PROJECT%_assert(...) clog_assert(__VA_ARGS__)
#define %PROJECT%_assert_msg(...) clog_assert_msg(__VA_ARGS__)


#else

#include <iostream>

#define %PROJECT%_error(msg, ...) std::cerr << "ERROR: " __FILE__ ":" << __LINE__ << ": " << msg << std::endl;
#define %PROJECT%_warning(msg, ...) std::cerr << "WARNING: " __FILE__ ":" << __LINE__ << ": " << msg << std::endl;
#define %PROJECT%_debug(msg, ...) std::cerr << "DEBUG: " __FILE__ ":" << __LINE__ << ": " << msg << std::endl;
#define %PROJECT%_info(msg, ...) std::cerr << "INFO: " __FILE__ ":" << __LINE__ << ": " << msg << std::endl;
#define %PROJECT%_fatal(msg, ...) std::cerr << "FATAL ERROR" __FILE__ ":" << __LINE__ << ": " << msg << std::endl; std::abort()

#define %PROJECT%_debug_vn(...) clog_debug_vn(__VA_ARGS__)

#if NDEBUG

#define %PROJECT%_assert(assrt) do { } while ((false) && (assrt))
#define %PROJECT%_assert_msg(assrt, msg, ...) %PROJECT%_assert(assrt)
#define %PROJECT%_near_equal_assert(v1, v2, tol) (void)(v1); (void)(v2); (void)(tol); do { } while (false)

#else

#define %PROJECT%_assert(assrt)                                     \
  if( not (assrt ) )                                                \
  {                                                                 \
    std::cerr << "assertion failed: " << #assrt << std::endl;       \
    std::abort();                                                   \
  }

#define %PROJECT%_assert_msg(assrt, msg, ...)                                    \
  if( not (assrt ) )                                                        \
  {                                                                         \
    std::cerr << "assertion failed: " << msg << " " << #assrt << std::endl; \
    std::abort();                                                           \
  }

#define %PROJECT%_near_equal_assert(v1, v2, tol)                                                          \
  {                                                                                                       \
    auto __clog__nea_ev1_ = (v1);                                                                         \
    auto __clog__nea_ev2_ = (v2);                                                                         \
    auto __clog__nea_tol_ = (tol);                                                                        \
    auto __clog__nea_error_ = std::abs(__clog__nea_ev1_ - __clog__nea_ev2_);                              \
    if(__clog__nea_error_ > __clog__nea_tol_)                                                             \
    {                                                                                                     \
      std::cerr << "Expected near equal: '" << __clog__nea_ev1_ << "' != '" << __clog__nea_ev2_           \
                << "' error: '" << __clog__nea_error_ << "', tolerance: '" << __clog__nea_tol_ << "'");   \
    }                                                                                                     \
  }


#endif

#endif
