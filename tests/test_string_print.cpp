#include <clog>
#include "catch.hpp"

TEST_CASE("string_format", "[string_format,to_string]")
{
  CHECK( fmt::format("hello") == "hello" );
  CHECK( fmt::format("{}", "hello") == "hello" );
  CHECK( fmt::format("{} world", "hello") == "hello world" );
  CHECK( fmt::format("{}", "hello") == "hello" );
  CHECK( fmt::format("{}{{}}", "hello") == "hello{}" );
  CHECK( fmt::format("{} world", "hello") == "hello world" );
  CHECK( fmt::format("{}{{}} world", "hello") == "hello{} world" );
  CHECK( fmt::format("{} {}", "hello", "world") == "hello world" );
}
