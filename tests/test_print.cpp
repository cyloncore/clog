#include <clog_print>

#include <sstream>

#include "catch.hpp"
#include "common.h"

TEST_CASE("clog_print", "[clog_print]")
{
  std::stringbuf sb;
  // Check that it prints to cout
  {
    cout_redirect cr(&sb);
    clog_print("hello");
  }
  CHECK_END_WITH(sb.str(), "hello\n" );
  // Check that it doesn't print to cout when redirecting to err
  {
    cout_redirect cr(&sb);
    clog_print<clog_print_flag::stderr>("world");
  }
  CHECK_END_WITH(sb.str(), "hello\n" );
  // Check that it prints to err
  {
    cerr_redirect cr(&sb);
    clog_print<clog_print_flag::stderr>(", I am clog.");
  }
  CHECK_END_WITH(sb.str(), ", I am clog.\n" );
  // Check that it inline arguments
  {
    cout_redirect cr(&sb);
    clog_print("I can count to {}.", 10);
  }
  CHECK_END_WITH(sb.str(), "I can count to 10.\n" );
  // Check that it doesn't print a new line
  {
    cout_redirect cr(&sb);
    clog_print<clog_print_flag::nonewline>("The library");
  }
  CHECK_END_WITH(sb.str(), "The library" );

  // Check that it handles inline arguments
  {
    cout_redirect cr(&sb);
    clog_print<clog_print_flag::force_tty>(" is {}blue{}.", clog_print_flag::blue, clog_print_flag::reset);
  }
  CHECK( ends_of(sb.str(), 19) == " is \033[34mblue\033[0m.\n" );

  // Check that it handles inline arguments
  {
    cout_redirect cr(&sb);
    clog_print("And not {}green{}.", clog_print_flag::green, clog_print_flag::reset);
  }
  CHECK_END_WITH(sb.str(), "And not green.\n");

}
