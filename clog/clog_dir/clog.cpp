#include "../clog"

namespace clog_impl
{
  void clog_report_error(const std::string& _source, const std::string& _msg)
  {
    clog_impl::logging_manager::report_error(_source, 0, _msg);
  }
  void __clog_fmt_force_symnbol__()
  {
    // Hack to force fmt symbols to be linked in
    fmt::format_error(std::string());
  }
}
