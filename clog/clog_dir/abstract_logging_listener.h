#ifndef _CLOG_ABSTRACT_LOGGING_LISTENER_H
#define _CLOG_ABSTRACT_LOGGING_LISTENER_H

#include <string>

#ifndef _CLOG_H_
#include "level.h"
#endif

namespace clog_impl
{
  class abstract_logging_listener
  {
  public:
    abstract_logging_listener();
    virtual ~abstract_logging_listener();
    virtual void report_debug(const std::string& _filename, int _line, const std::string& _message) = 0;
    virtual void report_error(const std::string& _filename, int _line, const std::string& _message) = 0;
    virtual void report_info(const std::string& _filename, int _line, const std::string& _message) = 0;
    virtual void report_fatal(const std::string& _filename, int _line, const std::string& _message) = 0;
    virtual void report_warning(const std::string& _filename, int _line, const std::string& _message) = 0;
  };
}

#endif
