#include "bench.h"

#include <clog>

#include <chrono>

using namespace clog_impl;

using std_clock = std::chrono::high_resolution_clock;
using nanoseconds = std::chrono::nanoseconds;

struct bench::data
{

  struct measurement
  {
    std::string name;
    std_clock::time_point start_time, end_time;
    std::vector<std::string> values;

    bool started = false;
  };

  measurement current;
  std::vector<std::string> columns;
  std::vector<measurement> measurements;

};

bench::bench() : d(new data)
{
}

bench::~bench()
{
  delete d;
}

void bench::setup_columns(const std::string& _name)
{
  d->columns.push_back(_name);
}

void bench::start_iteration_impl(const std::string& _name)
{
  clog_assert(not d->current.started);
  d->current.name = _name;
  d->current.started = true;
  d->current.start_time = std_clock::now();
}

void bench::end_iteration_impl()
{
  clog_assert(d->current.started);
  d->current.end_time = std_clock::now();

}

void bench::end_iteration_check()
{
  d->measurements.push_back(d->current);
  clog_assert(d->current.values.size() == d->columns.size());
  d->current = bench::data::measurement();
}

std::size_t bench::current_time() const
{
  clog_assert(d->current.started);
  return std::chrono::duration_cast<nanoseconds>(std_clock::now() - d->current.start_time).count();
}

void bench::add_value(const std::string& _v)
{
  clog_assert(d->current.started);
  d->current.values.push_back(_v);
}

void bench::print_results(std::ostream& _stream, bool _add_headers, const std::string& _seperator) const
{
  if(_add_headers)
  {
    _stream << "iteration" << _seperator << "time(ns)" << _seperator << "start(ns)" << _seperator << "end(ns)";
    for(const std::string& cn : d->columns)
    {
      _stream << _seperator << cn;
    }
    _stream << std::endl;
  }

  for(const data::measurement& m : d->measurements)
  {
    nanoseconds total_ns = std::chrono::duration_cast<nanoseconds>(m.end_time - m.start_time);
    nanoseconds start_ns = std::chrono::duration_cast<nanoseconds>(m.start_time.time_since_epoch());
    nanoseconds end_ns = std::chrono::duration_cast<nanoseconds>(m.end_time.time_since_epoch());
    _stream << m.name << _seperator << total_ns.count() << _seperator << start_ns.count() << _seperator << end_ns.count();

    for(const std::string& vs : m.values)
    {
      _stream << _seperator << vs;
    }
    _stream << std::endl;
  }
}
