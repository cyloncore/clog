
// The following macros are for internal use by clog, no warranty of behavior is guaranteed

#define __clog_inc(I) __clog_inc_## I
#define __clog_inc_0 1
#define __clog_inc_1 2
#define __clog_inc_2 3
#define __clog_inc_3 4
#define __clog_inc_4 5
#define __clog_inc_5 6
#define __clog_inc_6 7
#define __clog_inc_7 8
#define __clog_inc_8 9
#define __clog_inc_9 10
#define __clog_inc_10 11
#define __clog_inc_11 12
#define __clog_inc_12 13
#define __clog_inc_13 14
#define __clog_inc_14 15
#define __clog_inc_15 16
#define __clog_inc_16 17
#define __clog_inc_17 18
#define __clog_inc_18 19
#define __clog_inc_19 20
#define __clog_inc_20 21
#define __clog_inc_21 22
#define __clog_inc_22 23
#define __clog_inc_23 24
#define __clog_inc_24 25
#define __clog_inc_25 26
#define __clog_inc_26 27
#define __clog_inc_27 28
#define __clog_inc_28 29
#define __clog_inc_29 30
#define __clog_inc_30 31

#define __clog_foreach__1(WHAT, I, X) WHAT(X, I)
#define __clog_foreach__2(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__1(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__3(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__2(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__4(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__3(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__5(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__4(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__6(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__5(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__7(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__6(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__8(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__7(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__9(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__8(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__10(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__9(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__11(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__10(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__12(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__11(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__13(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__12(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__14(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__13(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__15(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__14(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__16(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__15(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__17(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__16(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__18(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__17(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__19(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__18(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__20(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__19(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__21(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__20(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__22(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__21(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__23(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__22(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__24(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__23(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__25(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__24(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__26(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__25(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__27(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__26(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__28(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__27(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__29(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__28(WHAT, __clog_inc(I), __VA_ARGS__)
#define __clog_foreach__30(WHAT, I, X, ...) WHAT(X, I)__clog_foreach__29(WHAT, __clog_inc(I), __VA_ARGS__)

#define __clog_get_macro(_1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14, _15, _16, _17, _18, _19, _20, _21, _22, _23, _24, _25, _26, _27, _28, _29, NAME, ...) NAME

#define __clog_foreach(action, ...) \
  __clog_get_macro(__VA_ARGS__, \
  __clog_foreach__29, __clog_foreach__28, __clog_foreach__27, __clog_foreach__26, __clog_foreach__25, __clog_foreach__24, __clog_foreach__23, __clog_foreach__22, __clog_foreach__21, __clog_foreach__20, \
  __clog_foreach__19, __clog_foreach__18, __clog_foreach__17, __clog_foreach__16, __clog_foreach__15, __clog_foreach__14, __clog_foreach__13, __clog_foreach__12, __clog_foreach__11, __clog_foreach__10, \
  __clog_foreach__9, __clog_foreach__8, __clog_foreach__7, __clog_foreach__6, __clog_foreach__5, __clog_foreach__4, __clog_foreach__3, __clog_foreach__2, __clog_foreach__1)(action, 0, __VA_ARGS__)

#define __clog_select(_WHAT_, ...) _WHAT_
