#include "stream_logging_listener.h"

#include <sstream>

#include <clog_print>

using namespace clog_impl;

struct stream_logging_listener::data
{
  std::ostream* stream;
  bool own_stream;
};

stream_logging_listener::stream_logging_listener(std::ostream* _stream, bool _own_stream) : d(new data{_stream, _own_stream})
{
}

stream_logging_listener::~stream_logging_listener()
{
  if(d->own_stream) delete d->stream;
  delete d;
}

namespace
{
  template<clog_print_flags _flags_>
  void report(std::ostream* _ostream,  const char* _level, const std::string& _filename, int _line, const std::string& _message)
  {
    std::string prefix = fmt::format("{}:{}: {}:", _filename, _line, _level);
    bool first_line = true;
    
    std::size_t current, previous = 0;
    current = _message.find('\n');
    while (current != std::string::npos) {
      clog_print(_ostream, "{}{}", prefix, _message.substr(previous, current - previous));
      if(first_line)
      {
        first_line = false;
        prefix = std::string(prefix.size() - 1, ' ') + ":";
      }
      previous = current + 1;
      current = _message.find('\n', previous);
    }
    clog_print<_flags_>(_ostream, "{}{}", prefix, _message.substr(previous, current - previous));
  }
}

void stream_logging_listener::report_error(const std::string& _filename, int _line, const std::string& _message)
{
  report<clog_print_flag::red>(d->stream, "error", _filename, _line, _message);
}

void stream_logging_listener::report_warning(const std::string& _filename, int _line, const std::string& _message)
{
  report<clog_print_flag::yellow>(d->stream, "warning", _filename, _line, _message);
}
void stream_logging_listener::report_info(const std::string& _filename, int _line, const std::string& _message)
{
  report<clog_print_flag::green>(d->stream, "info", _filename, _line, _message);
}
void stream_logging_listener::report_debug(const std::string& _filename, int _line, const std::string& _message)
{
  report<clog_print_flag::blue>(d->stream, "debug", _filename, _line, _message);
}

void stream_logging_listener::report_fatal(const std::string& _filename, int _line, const std::string& _message)
{
  report<clog_print_flag::red | clog_print_flag::bold>(d->stream, "fatal error", _filename, _line, _message);
}
