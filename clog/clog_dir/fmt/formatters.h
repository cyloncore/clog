#include "config.h"

#include <optional>
#include <unordered_map>
#include <vector>

namespace clog_fmt
{
  /**
   * Base for formatters with no formatting options.
   */
  struct base_formatter {
    template <typename FormatContext>
    constexpr auto parse(FormatContext& ctx)
    {
      auto it = ctx.begin(), end = ctx.end();
      
      if (it != end && *it != '}')
        throw fmt::format_error("invalid format");
      
      return it;
    }
    template<typename _T_, typename FormatContext>
    static auto forward(const _T_& p, FormatContext& ctx)
    {
      return fmt::formatter<_T_>().format(p, ctx);
    }
    
  };
  
  /**
   * Base for formatters that will capture the format, can be usefull for forwarding to an other formatter.
   */
  struct base_formatter_capture_options : public base_formatter {
    template <typename FormatContext>
    auto parse(FormatContext& ctx)
    {
      for(auto it = ctx.begin(); it != ctx.end(); ++it)
      {
        if(*it == '}')
        {
          return it;
        } else {
          format_str += *it;
        }
      }
      return ctx.end();
    }
  protected:
    std::string get_format_str() const { return format_str; }
  private:
    std::string format_str;
    
  };  
  /**
   * Define a formatter when formatting is done by casting to an other type.
   */
  template<typename _FROM_TYPE_, typename _TO_TYPE_>
  struct cast_formatter : public fmt::formatter<_TO_TYPE_> {
    template <typename FormatContext>
    auto format(const _FROM_TYPE_& p, FormatContext& ctx) CLOG_FMT_CONST
    {
      return fmt::formatter<_TO_TYPE_>::format(p, ctx);
    }
  };
  namespace details
  {
    template<typename _T_>
    struct force_print_data
    {
      force_print_data(const _T_& _data) : value(_data) {}
      const _T_& value;
    };
  }
  
  /**
   * This function can be used to force fmt to format certain types. For instance, fmt disallow formatting of constant pointer even if they
   * have a custom formatter, you can then use force_print to by pass that problem.
   * 
   * @code
   * struct CustomType;
   * 
   * struct fmt::formatter<const CustomType*> { ... };
   * 
   * const CustomType* ct = new CustomType();
   * fmt::to_string(ct); // triggers an error
   * fmt::to_string(clog::force_print(ct)); // works!
   * @endcode
   */
  template<typename _T_>
  details::force_print_data<_T_> force_print(_T_ const& _t)
  {
    static_assert(fmt::has_formatter<_T_, fmt::format_context>::value, "no formatter is defined for type");
    return details::force_print_data<_T_>(_t);
  }
}

template<typename _T_>
struct fmt::formatter<std::optional<_T_>> : public fmt::formatter<_T_>
{
  template <typename FormatContext>
  auto format(const std::optional<_T_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    if(p)
    {
      return fmt::formatter<_T_>::format(*p, ctx);
    } else {
      return fmt::format_to(ctx.out(), "null");
    }
  }
};

template<typename _T_>
struct fmt::formatter<std::vector<_T_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const std::vector<_T_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::format_to(ctx.out(), "{}", fmt::join(p, ", "));
  }
};

template<typename _T_, std::size_t _Nm_>
struct fmt::formatter<std::array<_T_, _Nm_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const std::array<_T_, _Nm_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::format_to(ctx.out(), "{}", fmt::join(p, ", "));
  }
};

template<typename _T1_, typename _T2_>
struct fmt::formatter<std::pair<_T1_, _T2_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const std::pair<_T1_, _T2_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::format_to(ctx.out(), "{}: {}", p.first, p.second);
  }
};

template<typename _K_, typename _T_>
struct fmt::formatter<std::unordered_map<_K_, _T_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const std::unordered_map<_K_, _T_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::format_to(ctx.out(), "{}", fmt::join(p, ", "));
  }
};

template<typename _T_>
struct fmt::formatter<clog_fmt::details::force_print_data<_T_>> : public fmt::formatter<_T_>
{
  template <typename FormatContext>
  auto format(const clog_fmt::details::force_print_data<_T_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::formatter<_T_>::format(p.value, ctx);
  }
};

#define __clog_fmt_declare_formatter_enum_case(_VALUE_, _I_)                        \
  case enum_type::_VALUE_:                                                          \
    return forward(# _VALUE_, ctx);

/**
 * Convenient macro for creating a formatter for enums.
 * 
 * @code
 * enum class MyEnum { Value1, Value2, Value3 };
 * 
 * clog_fmt_declare_formatter_enum(MyEnum, Value1, Value2, Value3);
 * @endcode
 */
#define clog_fmt_declare_formatter_enum(_TYPE_, ...)                                \
template<>                                                                          \
struct fmt::formatter<_TYPE_> : public clog_fmt::base_formatter {                   \
  template <typename FormatContext>                                                 \
  auto format(_TYPE_ const& p, FormatContext& ctx) CLOG_FMT_CONST -> decltype(ctx.out())  \
  {                                                                                 \
    using enum_type = _TYPE_;                                                       \
    switch(p)                                                                       \
    {                                                                               \
      __clog_foreach(__clog_fmt_declare_formatter_enum_case, __VA_ARGS__);          \
    }                                                                               \
    return fmt::format_to(ctx.out(), "Unhandled enum value {}", (int)p);            \
  }                                                                                 \
};

/**
 * Convenient macro for declaring formatters
 * 
 * @code
 * struct MyStruct { int a, b; };
 * clog_fmt_declare_formatter(scQL::Core::SymbolicObject::Field)
 * {
 *   return fmt::format_to(ctx.out(), "({}, {})", p.a, p.b);
 * }
 * @endcode
 */
#define clog_fmt_declare_formatter(_TYPE_, ...)                                     \
  template<>                                                                        \
  struct fmt::formatter<_TYPE_> : public __clog_select(__VA_ARGS__ __VA_OPT__(,) clog_fmt::base_formatter) {                 \
    template <typename FormatContext>                                               \
    auto format(_TYPE_ const& p, FormatContext& ctx) CLOG_FMT_CONST -> decltype(ctx.out()); \
  };                                                                                \
  template<typename FormatContext>                                                  \
  auto fmt::formatter<_TYPE_>::format(_TYPE_ const& p, FormatContext& ctx) CLOG_FMT_CONST-> decltype(ctx.out())

/**
 * Convenient macro for declaring formatters which inherits an other one.
 * 
 * @code
 * struct MyStruct { std::string a; };
 * clog_fmt_declare_formatter_inherit(MyStruct, std::string)
 * {
 *   return super::format(p.a, ctx);
 * }
 * @endcode
 */
#define clog_fmt_declare_formatter_inherit(_TYPE_, _OTHER_T_)                     \
template<>                                                                        \
struct fmt::formatter<_TYPE_> : public fmt::formatter<_OTHER_T_> {                \
  using super = fmt::formatter<_OTHER_T_>;                                        \
  template <typename FormatContext>                                               \
  auto format(_TYPE_ const& p, FormatContext& ctx) CLOG_FMT_CONST -> decltype(ctx.out()); \
};                                                                                \
template<typename FormatContext>                                                  \
auto fmt::formatter<_TYPE_>::format(_TYPE_ const& p, FormatContext& ctx) CLOG_FMT_CONST -> decltype(ctx.out())
