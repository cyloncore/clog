#pragma once

#include <fmt/core.h>

#if FMT_VERSION >= 90000
#define CLOG_FMT_CONST const
#else
#define CLOG_FMT_CONST
#endif