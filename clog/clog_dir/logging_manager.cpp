#include "logging_manager.h"

#include <cstdlib>
#include <iostream>
#include <mutex>
#include <vector>

#include "abstract_logging_listener.h"
#include "stream_logging_listener.h"

using namespace clog_impl;

struct logging_manager::data
{
  data() { listeners.push_back(new stream_logging_listener(&std::cerr, false)); }
  ~data() { for(abstract_logging_listener* l : listeners) { delete l; } }
  std::mutex mutex;
  std::vector<abstract_logging_listener*> listeners;
  int levels = (int)level::all;
  static data s_instance;
};

logging_manager::data logging_manager::data::s_instance;

void logging_manager::disable(level _level)
{
  std::lock_guard<std::mutex> l(data::s_instance.mutex);
  data::s_instance.levels &= ~int(_level);
}

void logging_manager::enable(level _level)
{
  std::lock_guard<std::mutex> l(data::s_instance.mutex);
  data::s_instance.levels |= int(_level);
}

bool logging_manager::is_enabled(level _level)
{
  std::lock_guard<std::mutex> l(data::s_instance.mutex);
  return (data::s_instance.levels & int(_level)) == int(_level);
}

bool logging_manager::is_enabled(const char*, clog_impl::level _level)
{
  return is_enabled(_level);
}

void logging_manager::add_listener(abstract_logging_listener* _listener)
{
  std::lock_guard<std::mutex> l(data::s_instance.mutex);
  data::s_instance.listeners.push_back(_listener);
}

void logging_manager::remove_all_listener()
{
  std::lock_guard<std::mutex> l(data::s_instance.mutex);
  for(abstract_logging_listener* l : data::s_instance.listeners)
  {
    delete l;
  }
  data::s_instance.listeners.clear();
}

std::string logging_manager::build_message(std::size_t _count)
{
  std::string r;
  for(std::size_t i = 0; i < _count; ++i)
  {
    r += "{" + std::to_string(i) + "}";
    if(i < _count - 1) r += " ";
  }
  return r;
}

void logging_manager::report_debug(const std::string& _filename, int _line, const std::string& _message)
{
  if(is_enabled(level::debug))
  {
    std::lock_guard<std::mutex> l(data::s_instance.mutex);
    for(abstract_logging_listener* l : data::s_instance.listeners)
    {
      l->report_debug(_filename, _line, _message);
    }
  }
}

void logging_manager::report_info(const std::string& _filename, int _line, const std::string& _message)
{
  if(is_enabled(level::info))
  {
    std::lock_guard<std::mutex> l(data::s_instance.mutex);
    for(abstract_logging_listener* l : data::s_instance.listeners)
    {
      l->report_info(_filename, _line, _message);
    }
  }
}

void logging_manager::report_error(const std::string& _filename, int _line, const std::string& _message)
{
  if(is_enabled(level::error))
  {
    std::lock_guard<std::mutex> l(data::s_instance.mutex);
    for(abstract_logging_listener* l : data::s_instance.listeners)
    {
      l->report_error(_filename, _line, _message);
    }
  }
}

void logging_manager::report_warning(const std::string& _filename, int _line, const std::string& _message)
{
  if(is_enabled(level::warning))
  {
    std::lock_guard<std::mutex> l(data::s_instance.mutex);
    for(abstract_logging_listener* l : data::s_instance.listeners)
    {
      l->report_warning(_filename, _line, _message);
    }
  }
}

void logging_manager::report_fatal(const std::string& _filename, int _line, const std::string& _message)
{
  std::lock_guard<std::mutex> l(data::s_instance.mutex);
  for(abstract_logging_listener* l : data::s_instance.listeners)
  {
    l->report_fatal(_filename, _line, _message);
  }
  std::abort();
}
