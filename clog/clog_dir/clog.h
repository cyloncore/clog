#ifndef _CLOG_H_
#define _CLOG_H_

#include <fstream>
#include <string>

#define __CLOG_INTERNAL_STRINGIFY(...) #__VA_ARGS__

namespace clog_impl
{
  void clog_report_error(const std::string& _source, const std::string& _msg);
}

#include "config.h"

#include "macros.h"

#include "level.h"

#include "abstract_logging_listener.h"
#include "stream_logging_listener.h"

#include "logging_manager.h"

#include "decomposer.h"
#include "logging.h"
#include "handle_assert.h"
#include "utils.h"

using clog_abstract_logging_listener = clog_impl::abstract_logging_listener;

inline void clog_stream_to_file(const std::string& _name)
{
  clog_impl::logging_manager::add_listener(new clog_impl::stream_logging_listener(new std::ofstream(_name), true));
}

inline void clog_remove_all_listener()
{
  clog_impl::logging_manager::remove_all_listener();
}

#ifdef CLOG_HAS_SQLITE

#include "sqlite_logging_listener.h"

inline void clog_stream_to_sqlite(const std::string& _name)
{
  clog_impl::logging_manager::add_listener(new clog_impl::sqlite_logging_listener(_name));
}

#endif

#endif
