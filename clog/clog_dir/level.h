#ifndef _CLOG_LEVEL_H_
#define _CLOG_LEVEL_H_

namespace clog_impl
{
  enum class level
  {
    info = 0x1,
    debug = 0x2,
    warning = 0x4,
    error = 0x8,
    all = info | debug | warning | error
  };
}

#endif
