#include <fmt/format.h>

#include "../fmt/config.h"

#include <QDebug>

#include <QCborValue>
#include <QDateTime>
#include <QJsonValue>
#include <QList>
#include <QPointF>
#include <QUuid>
#include <QVariant>
#include <QSharedPointer>

template<>
struct fmt::formatter<QPoint> : public fmt::formatter<int>
{
  template <typename FormatContext>
  auto format(const QPoint& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return format_to(ctx.out(),
                     "({}, {})",
                     fmt::formatter<int>::format(p.x(), ctx), fmt::formatter<int>::format(p.y(), ctx));
  }
};

template<>
struct fmt::formatter<QPointF> : public fmt::formatter<qreal>
{
  template <typename FormatContext>
  auto format(const QPointF& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return format_to(ctx.out(),
                     "({}, {})",
                     fmt::formatter<qreal>::format(p.x(), ctx), fmt::formatter<qreal>::format(p.y(), ctx));
  }
};

namespace clog_qt::details
{
  namespace ci = clog_impl;
  template<typename T>
  inline constexpr bool is_qt_smart_pointer_v = ci::is_instantiation_of_v<QExplicitlySharedDataPointer, T> or ci::is_instantiation_of_v<QSharedPointer, T>;
}

template<typename _T_, typename Char >
struct fmt::formatter<_T_, Char, std::enable_if_t<clog_qt::details::is_qt_smart_pointer_v<_T_>>> : clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const _T_& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return forward(p.data(), ctx);
  }
};

template<typename _T_>
struct clog_impl::map_for_expr_printing<_T_, std::enable_if_t<clog_qt::details::is_qt_smart_pointer_v<_T_>>>
{
  static const void* map(const _T_& _t)
  {
    return _t.data();
  }
};

template<>
struct fmt::formatter<QString> : fmt::formatter<std::string> {
  template <typename FormatContext>
  auto format(const QString& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::formatter<std::string>::format(p.toStdString(), ctx);
  }
};

template<>
struct fmt::formatter<QByteArray> : fmt::formatter<QString> {
  template <typename FormatContext>
  auto format(const QByteArray& p, FormatContext& ctx)
  {
    return fmt::formatter<QString>::format(p.toHex(), ctx);
  }
};

namespace clog_qt::details
{
  template<class>
  struct sfinae_true : std::true_type{};

  template<class T>
  static auto test_toString(int)
      -> sfinae_true<decltype(std::declval<T>().toString())>;
  template<class>
  static auto test_toString(long) -> std::false_type;

  template<class T>
  struct has_toString : decltype(test_toString<T>(0)){};
}

static_assert(clog_qt::details::has_toString<QStringRef>::value);
static_assert(clog_qt::details::has_toString<QUuid>::value);
static_assert(clog_qt::details::has_toString<QVariant>::value);

template<typename _T_, typename Char >
struct fmt::formatter<_T_, Char, std::enable_if_t<clog_qt::details::has_toString<_T_>::value>> : fmt::formatter<QString> {
  template <typename FormatContext>
  auto format(const _T_& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::formatter<QString>::format(p.toString(), ctx);
  }
};

template<>
struct fmt::formatter<QChar> : fmt::formatter<QString> {
  template <typename FormatContext>
  auto format(const QChar& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::formatter<QString>::format(QString(p), ctx);
  }
};

template<>
struct fmt::formatter<QCharRef> : fmt::formatter<QChar> {
};

template<>
struct fmt::formatter<QDateTime> : fmt::formatter<QString>
{
  template <typename FormatContext>
  auto format(const QDateTime& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return fmt::formatter<QString>::format(p.toString(Qt::ISODate), ctx);
  }
};

template<typename _T1_, typename _T2_>
struct fmt::formatter<QPair<_T1_, _T2_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const QPair<_T1_, _T2_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return format_to(ctx.out(), "{{{}, {}}}", p.first, p.second);
  }
};

template<typename _T_>
struct fmt::formatter<QList<_T_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const QList<_T_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    return format_to(ctx.out(), "{}", fmt::join(p, ", "));
  }
};

template<typename _K_, typename _V_>
struct fmt::formatter<QHash<_K_, _V_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const QHash<_K_, _V_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    QStringList list;
    for(auto it = p.begin(); it != p.end(); ++it)
    {
      list.append(clog_qt::qformat("{}: {}", it.key(), it.value()));
    }
    return format_to(ctx.out(), "{}", fmt::join(p, ", "));
  }
};

template<typename _K_, typename _V_>
struct fmt::formatter<QMap<_K_, _V_>> : public clog_fmt::base_formatter
{
  template <typename FormatContext>
  auto format(const QMap<_K_, _V_>& p, FormatContext& ctx) CLOG_FMT_CONST
  {
    QStringList list;
    for(auto it = p.begin(); it != p.end(); ++it)
    {
      list.append(clog_qt::qformat("{}: {}", it.key(), it.value()));
    }
    return format_to(ctx.out(), "{}", fmt::join(p, ", "));
  }
};

template<>
struct fmt::formatter<QStringList> : public fmt::formatter<QList<QString>>
{
};

namespace clog_qt::details
{
  template<typename _T_>
  struct qdebug_fromatter : public fmt::formatter<QString>
  {
    template <typename FormatContext>
    auto format(const _T_& p, FormatContext& ctx) CLOG_FMT_CONST
    {
      QString errMsg;
      QDebug err(&errMsg);
      err.nospace() << p;
      return fmt::formatter<QString>::format(errMsg, ctx);
    }
  };
}

#define CLOG_QT_DEBUG_FORMATTER(_TYPE_)                                                 \
  template<>                                                                            \
  struct fmt::formatter<_TYPE_> : public clog_qt::details::qdebug_fromatter<_TYPE_> {};

CLOG_QT_DEBUG_FORMATTER(QCborValue)
CLOG_QT_DEBUG_FORMATTER(QJsonValue)
CLOG_QT_DEBUG_FORMATTER(QJsonObject)
CLOG_QT_DEBUG_FORMATTER(QJsonDocument)

#undef CLOG_QT_DEBUG_FORMATTER
