#include <fmt/format.h>

namespace clog_qt
{
#if FMT_VERSION >= 80000
  template <typename... Args>
  inline std::string format(QString const& format_str, Args&&... args) {
    return ::fmt::vformat(format_str.toStdString(), fmt::make_format_args(args...));
  }
#else
  template <typename... Args>
  inline std::string format(QString const& format_str, Args&&... args) {
    return ::fmt::format(format_str.toStdString(), args...);
  }
#endif
  template <typename... Args>
  inline std::string vformat(QString const& format_str, Args&&... args) {
    return ::fmt::vformat(format_str.toStdString(), args...);
  }
  inline QString qformat(QString const& format_str) {
    return format_str;
  }  
  template <typename... Args>
  inline QString qformat(QString const& format_str, Args&&... args) {
    return QString::fromStdString(format(format_str, args...));
  }
  template <typename... Args>
  inline QString qvformat(QString const& format_str, Args&&... args) {
    return QString::fromStdString(vformat(format_str, args...));
  }
  template <typename T>
  inline QString to_qstring(T const& value) {
    return qformat("{}", value);
  }
}

#include <QDebug>

template<typename _T_, std::enable_if_t<fmt::has_formatter<_T_, fmt::format_context>::value, bool> = true>
QDebug operator<<(QDebug _debug, _T_ const&_object)
{
  _debug << clog_qt::to_qstring(_object);
  return _debug;
}
