#include <cstdint>

#include <unistd.h>
#include <stdio.h>
#include <iostream>

namespace
{
  std::streambuf const *coutbuf = std::cout.rdbuf();
  std::streambuf const *cerrbuf = std::cerr.rdbuf();
}

namespace clog_impl::print
{
  struct flags
  {
    constexpr flags(uint64_t _v) : value(_v)
    {

    }
    const uint64_t value;
  };
  constexpr flags operator&(const flags& _lhs, const flags& _rhs)
  {
    return _lhs.value & _rhs.value;
  }
  constexpr flags operator|(const flags& _lhs, const flags& _rhs)
  {
    return _lhs.value | _rhs.value;
  }
  namespace flag
  {
    static constexpr flags empty = 0x0;
    static constexpr flags reset = 0x1 << 1;
    static constexpr flags stdout = 0x1 << 2;
    static constexpr flags stderr = 0x1 << 3;
    static constexpr flags force_tty = 0x1 << 4;
    static constexpr flags nonewline = 0x1 << 5;
    static constexpr flags default_color = 0x1 << 6;
    static constexpr flags red = 0x1 << 7;
    static constexpr flags green = 0x1 << 8;
    static constexpr flags yellow = 0x1 << 9;
    static constexpr flags blue = 0x1 << 10;
    static constexpr flags bold = 0x1 << 11;
    static constexpr flags italic = 0x1 << 12;
  };

  inline bool is_atty(std::ostream &s)
  {
    return ( (s.rdbuf() == std::cout.rdbuf() and coutbuf == std::cout.rdbuf() and isatty(fileno(stdout)))
          or (s.rdbuf() == std::cerr.rdbuf() and cerrbuf == std::cerr.rdbuf() and isatty(fileno(stderr))));
  }
  static const char* reset_format = "\033[0m";
  static const char* red_format = "\033[31m";
  static const char* green_format = "\033[32m";
  static const char* yellow_format = "\033[33m";
  static const char* blue_format = "\033[34m";
  static const char* default_color_format = "\033[39m";
  static const char* bold_format = "\033[1m";
  static const char* italic_format = "\033[3m";
  
  template<flags _flags_, flags _p_>
  constexpr bool has_flag()
  {
    return (_flags_ & _p_).value;
  }
  template<flags _p_>
  constexpr bool has_flag(const flags& _flags_)
  {
    return (_flags_ & _p_).value;
  }
  template<flags _flags_, flags _p_>
  constexpr void apply_io(std::ostream* _stream, const char* _format)
  {
    if(has_flag<_flags_, _p_>() and is_atty(*_stream))
    {
      *_stream << _format;
    }
  }

 
  template<typename _T_>
  requires (not std::same_as<_T_, flags>)
  inline const _T_& filter(bool, const _T_& _t)
  {
    return _t;
  }
  inline const flags& filter(bool _is_atty, const flags& _t)
  {
    return _is_atty ? _t : flag::empty;
  }
}

namespace clog_print_flag = clog_impl::print::flag;
using clog_print_flags = clog_impl::print::flags;

template<>
struct fmt::formatter<clog_print_flags> {
  template <typename FormatContext>
  constexpr auto parse(FormatContext& ctx) 
  {
    auto it = ctx.begin(), end = ctx.end();
    
    if (it != end && *it != '}')
      throw fmt::format_error("invalid format");
    
    return it;
  }
  template<clog_print_flags _flags_, typename _TIt_>
  constexpr auto apply(_TIt_ _it, clog_print_flags _p, const char* _format)
  {
    if((_p & _flags_).value)
    {
      return fmt::format_to(_it, "{}", _format);
    } else {
      return _it;
    }
  }

  template <typename FormatContext>
  auto format(clog_print_flags const& p, FormatContext& ctx) CLOG_FMT_CONST -> decltype(ctx.out())
  {
    using namespace clog_impl::print;
    auto it = ctx.out();
    it = apply<clog_print_flag::default_color>(it, p, default_color_format);
    it = apply<clog_print_flag::red>(it, p, red_format);
    it = apply<clog_print_flag::green>(it, p, green_format);
    it = apply<clog_print_flag::yellow>(it, p, yellow_format);
    it = apply<clog_print_flag::blue>(it, p, blue_format);
    it = apply<clog_print_flag::bold>(it, p, bold_format);
    it = apply<clog_print_flag::italic>(it, p, italic_format);
    it = apply<clog_print_flag::reset>(it, p, reset_format);
    return it;
  }
  
};


template<clog_print_flags _flags_ = clog_print_flag::stdout, typename... _T_>
inline void clog_print(std::ostream* _ostream, const ::fmt::format_string<_T_...>& _message, _T_... _values)
{
  using namespace clog_impl::print;
  apply_io<_flags_, clog_print_flag::italic>(_ostream, italic_format);
  apply_io<_flags_, clog_print_flag::bold>(_ostream, bold_format);
  apply_io<_flags_, clog_print_flag::red>(_ostream, red_format);
  apply_io<_flags_, clog_print_flag::blue>(_ostream, blue_format);
  apply_io<_flags_, clog_print_flag::green>(_ostream, green_format);
  *_ostream << fmt::vformat(_message, std::apply(&fmt::make_format_args<fmt::format_context, _T_...>,
    std::make_tuple(clog_impl::print::filter(is_atty(*_ostream) or has_flag<_flags_, clog_print_flag::force_tty>(), _values)...)));
  if(is_atty(*_ostream))
  {
    *_ostream << reset_format;
  }
  if constexpr(not has_flag<_flags_, clog_print_flag::nonewline>())
  {
    *_ostream << std::endl;
  }
}

template<clog_print_flags _flags_ = clog_print_flag::stdout, typename... _T_>
inline void clog_print(const ::fmt::format_string<_T_...>& _message, _T_... _values)
{
  using namespace clog_impl::print;
  if constexpr(has_flag<_flags_, clog_print_flag::stderr>())
  {
    clog_print<_flags_>(&std::cerr, _message, _values...);
  } else {
    clog_print<_flags_>(&std::cout, _message, _values...);
  }
}
