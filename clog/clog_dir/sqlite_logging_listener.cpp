#include "sqlite_logging_listener.h"

#include <sqlite3.h>

#include <cstring>
#include <iostream>

#include <chrono>

using namespace clog_impl;

struct sqlite_logging_listener::data
{
  sqlite3* handle = nullptr;
  void report_last_error(const std::string& _query)
  {
    std::cout << "Failed to execute query: '" << _query << "' with error: '" << sqlite3_errmsg(handle) << "'" << std::endl;
  }
  void report(int _level, const std::string& _filename, int _line, const std::string& _message)
  {
    if(handle)
    {
      const char* query = R"""(INSERT INTO clog_logs (level, timesamp, filename, line, message) VALUES (?001, ?002, ?003, ?004, ?005))""";
      sqlite3_stmt* ps;
      if(sqlite3_prepare_v2(handle, query, strlen(query), &ps, nullptr) != SQLITE_OK)
      {
        sqlite3_finalize(ps);
        report_last_error(query);
      } else {
        sqlite3_bind_int64(ps, 1, _level);
        sqlite3_bind_int64(ps, 2, std::chrono::time_point_cast<std::chrono::nanoseconds>(std::chrono::system_clock::now()).time_since_epoch().count());
        sqlite3_bind_text(ps, 3, _filename.c_str(), _filename.size(), nullptr);
        sqlite3_bind_int64(ps, 4, _line);
        sqlite3_bind_text(ps, 5, _message.c_str(), _message.size(), nullptr);
        if(sqlite3_step(ps) != SQLITE_DONE)
        {
          sqlite3_finalize(ps);
          report_last_error(query);
        }
      }

    } else {
      std::cerr << "Failure to log in database, database not opened." << std::endl;
    }


  }
};

sqlite_logging_listener::sqlite_logging_listener(const std::string& _database) : d(new data)
{
  if(sqlite3_open(_database.c_str(), &d->handle) != SQLITE_OK)
  {
    std::cerr << "Failed to open database '" << _database << "' for logging with error: '" << sqlite3_errmsg(d->handle) << "'" << std::endl;
    sqlite3_close(d->handle);
    d->handle = nullptr;
  } else {
    const char* query = R"""(CREATE TABLE IF NOT EXISTS clog_logs(id INTEGER PRIMARY KEY AUTOINCREMENT,
        level INTEGER NOT NULL,
        timesamp INTEGER NOT NULL,
        filename TEXT NOT NULL,
        line INTEGER NOT NULL,
        message TEXT NOT NULL
        );)""";
    sqlite3_stmt* ps;
    if(sqlite3_prepare_v2(d->handle, query, strlen(query), &ps, nullptr) != SQLITE_OK)
    {
      sqlite3_finalize(ps);
      d->report_last_error(query);
      sqlite3_close(d->handle);
      d->handle = nullptr;
    }
    if(sqlite3_step(ps) != SQLITE_DONE)
    {
      sqlite3_finalize(ps);
      d->report_last_error(query);
      sqlite3_close(d->handle);
      d->handle = nullptr;
    } else {
      sqlite3_finalize(ps);
    }
  }
}

sqlite_logging_listener::~sqlite_logging_listener()
{
  if(d->handle)
  {
    sqlite3_close(d->handle);
  }
  delete d;
}

void sqlite_logging_listener::report_debug(const std::string& _filename, int _line, const std::string& _message)
{
  d->report(0, _filename, _line, _message);
}

void sqlite_logging_listener::report_error(const std::string& _filename, int _line, const std::string& _message)
{
  d->report(1, _filename, _line, _message);
}

void sqlite_logging_listener::report_info(const std::string& _filename, int _line, const std::string& _message)
{
  d->report(2, _filename, _line, _message);
}

void sqlite_logging_listener::report_fatal(const std::string& _filename, int _line, const std::string& _message)
{
  d->report(3, _filename, _line, _message);
}

void sqlite_logging_listener::report_warning(const std::string& _filename, int _line, const std::string& _message)
{
  d->report(4, _filename, _line, _message);
}
