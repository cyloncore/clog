#include <iostream>

#include <chrono>

#include <clog>

namespace clog_impl
{
  /**
   * @internal
   * @ingroup clog_chrono
   *
   * This class is used to measure time
   */
  class chrono
  {
    using clock = std::chrono::high_resolution_clock;
    using milliseconds = std::chrono::milliseconds;
  public:
    chrono() : chrono(nullptr, 0, nullptr) {
    }
    chrono(const char* _filename, int _line, const char* _name) : filename(_filename), line(_line), text(_name)
    {
      t0 = clock::now();
    }
#if FMT_VERSION >= 80000
    template<typename... _T_>
    chrono(const char* _filename, int _line, const char* _name, const fmt::format_string<_T_...>& _msg, const _T_&... _data) : filename(_filename), line(_line), text(_name), m_msg(fmt::vformat(_msg, fmt::make_format_args(_data...)))
    {
      t0 = clock::now();
    }
#else
    template<typename... _T_>
    chrono(const char* _filename, int _line, const char* _name, const char* _msg, const _T_&... _data) : filename(_filename), line(_line), text(_name), m_msg(fmt::format(_msg, _data...))
    {
      t0 = clock::now();
    }
    template<typename... _T_>
    chrono(const char* _filename, int _line, const char* _name, const std::string& _msg, const _T_&... _data) : filename(_filename), line(_line), text(_name), m_msg(fmt::format(_msg, _data...))
    {
      t0 = clock::now();
    }
#endif
    ~chrono()
    {
      if(filename and text)
      {
        if(m_msg.empty())
        {
          clog_impl::logging_manager::report_debug(filename, line, "chrono({}): {} ms", text, current<milliseconds>());
        } else {
          clog_impl::logging_manager::report_debug(filename, line, "chrono({}): {} ms: {}", text, current<milliseconds>(), m_msg);
        }
      }
    }
    void report(const std::string& _msg)
    {
      if(m_msg.empty())
      {
        clog_impl::logging_manager::report_debug(filename, line, "chrono({}): {} ms: {}", text, current<milliseconds>(), _msg);
      } else {
        clog_impl::logging_manager::report_debug(filename, line, "chrono({}): {} ms: {}: {}", text, current<milliseconds>(), m_msg, _msg);
      }
    }
    template<typename _unit_>
    int current() const
    {
      clock::time_point t1 = clock::now();
      _unit_ total_ms = std::chrono::duration_cast<_unit_>(t1 - t0);
      return total_ms.count();
    }
  private:
    clock::time_point t0;
    const char* filename;
    int line;
    const char* text;
    std::string m_msg;
  };
}

/**
 * @ingroup clog_chrono
 * Define a new chrono with the given @p _name_ (this should be a valid C++ identifier).
 * When it goes out of scope, it will display the elapsed time.
 *
 * @code
 * clog_chrono(my_chrono);
 * @endcode
 */
#define clog_chrono(_name_) clog_impl::chrono __clog__chrono__ ## _name_(__FILE__, __LINE__, #_name_)

/**
 * @ingroup clog_chrono
 * Define a new chrono with the given @p _name_ (this should be a valid C++ identifier).
 * When it goes out of scope, it will display the elapsed time and the message @p _msg_.
 *
 * @code
 * clog_chrono_msg(my_chrono, "A msg {}", 10);
 * @endcode
 */
#define clog_chrono_msg(_name_, _msg_, ...) clog_impl::chrono __clog__chrono__ ## _name_(__FILE__, __LINE__, #_name_, (_msg_), __VA_ARGS__)

/**
 * @ingroup clog_chrono
 *
 * Force to report the chrono identified with @p _name_
 *
 * @code
 * clog_chrono(my_chrono);
 * ...
 * clog_chrono_report(my_chrono, "intermediary step");
 * @endcode
 */
#define clog_chrono_report(_name_, _msg_) __clog__chrono__ ## _name_.report(_msg_)

/**
 * @ingroup clog_chrono
 *
 * Get the current value of a chrono in _unit_
 *
 * @code
 * clog_chrono(my_chrono);
 * ...
 * if(clog_chrono_current(my_chrono, std::chrono::nanoseconds) > 5)
 * ...
 * @endcode
 */
#define clog_chrono_current(_name_, _unit_) __clog__chrono__ ## _name_.current<_unit_>()

/**
 * @ingroup clog_chrono
 *
 * Get the current value of a chrono in ms
 *
 * @code
 * clog_chrono(my_chrono);
 * ...
 * if(clog_chrono_current_ms(my_chrono) > 5)
 * ...
 * @endcode
 */
#define clog_chrono_current_ms(_name_) clog_chrono_current(_name_, std::chrono::milliseconds)
