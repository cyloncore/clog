
namespace clog_impl
{
  /**
   * fmt::format does not allow to print null string, if that is something that can happen, use this function
   * to convert the null string to a non null one.
   */
  inline const char* safe_string(const char* _string, const char* _for_null = "(null string)")
  {
    clog_assert(_for_null);
    return _string ? _string : _for_null;
  }
}


template<typename... _T_>
auto clog_safe_string(const _T_&... _args)
{
  return clog_impl::safe_string(_args...);
}
