#ifndef _CLOG_LOGGING_MANAGER_H_
#define _CLOG_LOGGING_MANAGER_H_

#ifndef _CLOG_H_
#include "abstract_logging_listener.h"
#include "level.h"
#include <fmt/core.h>
#endif

#ifndef CLOG_SHOULD_REPORT_ERRORS
#define CLOG_SHOULD_REPORT_ERRORS true
#endif

#ifndef CLOG_SHOULD_REPORT_WARNINGS
#define CLOG_SHOULD_REPORT_WARNINGS true
#endif

#ifndef CLOG_SHOULD_REPORT_INFOS
#define CLOG_SHOULD_REPORT_INFOS true
#endif

#ifndef CLOG_SHOULD_REPORT_DEBUGS
#define CLOG_SHOULD_REPORT_DEBUGS true
#endif

#ifdef CLOG_DISABLE_ALL_REPORT
#define CLOG_SHOULD_REPORT_ERRORS false
#define CLOG_SHOULD_REPORT_WARNINGS false
#define CLOG_SHOULD_REPORT_DEBUGS false
#endif

namespace clog_impl
{
  class logging_manager
  {
  public:
    static void add_listener(abstract_logging_listener* _listener);
    static void remove_all_listener();
    static void disable(level _level);
    static void enable(level _level);
    static bool is_enabled(level _level);
  private:
    static bool is_enabled(const char* _filename, level _level);
  public:
    static bool should_report_errors(const char* _filename) { return CLOG_SHOULD_REPORT_ERRORS and is_enabled(_filename, level::error); }
    static bool should_report_debugs(const char* _filename) { return CLOG_SHOULD_REPORT_DEBUGS and is_enabled(_filename, level::debug); }
    static bool should_report_infos(const char* _filename) { return CLOG_SHOULD_REPORT_INFOS and is_enabled(_filename, level::info); }
    static bool should_report_warnings(const char* _filename) { return CLOG_SHOULD_REPORT_WARNINGS and is_enabled(_filename, level::warning); }
  public:
    static void report_error(const std::string& _filename, int _line, const std::string& _message);
    static void report_warning(const std::string& _filename, int _line, const std::string& _message);
    static void report_debug(const std::string& _filename, int _line, const std::string& _message);
    static void report_info(const std::string& _filename, int _line, const std::string& _message);
    [[noreturn]] static void report_fatal(const std::string& _filename, int _line, const std::string& _message);
#if FMT_VERSION >= 80000
    template<typename... _T_>
    static void report_error(const std::string& _filename, int _line, const fmt::format_string<_T_...>& _message, _T_... _values)
    {
      report_error(_filename, _line, fmt::vformat(_message, fmt::make_format_args(_values...)));
    }
    template<typename... _T_>
    static void report_warning(const std::string& _filename, int _line, const fmt::format_string<_T_...>& _message, _T_... _values)
    {
      report_warning(_filename, _line, fmt::vformat(_message, fmt::make_format_args(_values...)));
    }
    template<typename... _T_>
    static void report_debug(const std::string& _filename, int _line, const fmt::format_string<_T_...>& _message, _T_... _values)
    {
      report_debug(_filename, _line, fmt::vformat(_message, fmt::make_format_args(_values...)));
    }
    template<typename... _T_>
    static void report_info(const std::string& _filename, int _line, const fmt::format_string<_T_...>& _message, _T_... _values)
    {
      report_info(_filename, _line, fmt::vformat(_message, fmt::make_format_args(_values...)));
    }
    template<typename... _T_>
    [[noreturn]] static void report_fatal(const std::string& _filename, int _line, const fmt::format_string<_T_...>& _message, _T_... _values)
    {
      report_fatal(_filename, _line, fmt::vformat(_message, fmt::make_format_args(_values...)));
    }
#else
template<typename... _T_>
    static void report_error(const std::string& _filename, int _line, const std::string& _message, _T_... _values)
    {
      report_error(_filename, _line, fmt::format(_message, _values...));
    }
    template<typename... _T_>
    static void report_warning(const std::string& _filename, int _line, const std::string& _message, _T_... _values)
    {
      report_warning(_filename, _line, fmt::format(_message, _values...));
    }
    template<typename... _T_>
    static void report_debug(const std::string& _filename, int _line, const std::string& _message, _T_... _values)
    {
      report_debug(_filename, _line, fmt::format(_message, _values...));
    }
    template<typename... _T_>
    static void report_info(const std::string& _filename, int _line, const std::string& _message, _T_... _values)
    {
      report_info(_filename, _line, fmt::format(_message, _values...));
    }
    template<typename... _T_>
    [[noreturn]] static void report_fatal(const std::string& _filename, int _line, const std::string& _message, _T_... _values)
    {
      report_fatal(_filename, _line, fmt::format(_message, _values...));
    }
#endif
  private:
    static std::string build_message(std::size_t _count);
  private:
    struct data;
  };
}

#endif

