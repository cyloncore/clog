set(CLOG_SRCS
  clog_dir/abstract_logging_listener.cpp
  clog_dir/clog.cpp
  clog_dir/logging_manager.cpp
  clog_dir/stream_logging_listener.cpp)

set(CLOG_LIBS fmt::fmt)

set(CLOG_HAS_SQLITE ${SQLite3_FOUND})
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/clog_dir/config.h.in ${CMAKE_CURRENT_BINARY_DIR}/clog_dir/config.h)

if(CLOG_HAS_SQLITE)
  set(CLOG_SRCS ${CLOG_SRCS}
    clog_dir/sqlite_logging_listener.cpp)
  set(CLOG_LIBS ${CLOG_LIBS} SQLite::SQLite3)
endif()

add_library(clog SHARED ${CLOG_SRCS})
target_link_libraries(clog PUBLIC ${CLOG_LIBS})

set(CLOG_BENCH_SRCS
  clog_dir/bench.cpp)

add_library(clog_bench SHARED ${CLOG_BENCH_SRCS})
target_link_libraries(clog_bench PUBLIC clog)

install(TARGETS clog clog_bench EXPORT clogTargets ${INSTALL_TARGETS_DEFAULT_ARGS} )

install(FILES clog_bench clog clog_fmt clog_qt clog_chrono clog_print DESTINATION ${INSTALL_INCLUDE_DIR}/ ${CLOG_INSTALL_PERMISSIONS} )
install(FILES clog_dir/abstract_logging_listener.h
              clog_dir/bench.h
              clog_dir/clog.h
              clog_dir/chrono.h
              clog_dir/decomposer.h
              clog_dir/handle_assert.h
              clog_dir/level.h
              clog_dir/logging.h
              clog_dir/logging_manager.h
              clog_dir/macros.h
              clog_dir/stream_logging_listener.h
              clog_dir/utils.h
    DESTINATION ${INSTALL_INCLUDE_DIR}/clog_dir ${CLOG_INSTALL_PERMISSIONS} )

# config.h header

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/clog_dir/config.h
    DESTINATION ${INSTALL_INCLUDE_DIR}/clog_dir ${CLOG_INSTALL_PERMISSIONS} )

# sqlite headers
if(CLOG_HAS_SQLITE)
  install(FILES clog_dir/sqlite_logging_listener.h
      DESTINATION ${INSTALL_INCLUDE_DIR}/clog_dir ${CLOG_INSTALL_PERMISSIONS} )
endif()

# clog_fmt headers
install(FILES clog_dir/fmt/formatters.h clog_dir/fmt/config.h
    DESTINATION ${INSTALL_INCLUDE_DIR}/clog_dir/fmt ${CLOG_INSTALL_PERMISSIONS} )

# clog_qt headers
install(FILES clog_dir/qt/fmt_formatter.h
              clog_dir/qt/format.h
    DESTINATION ${INSTALL_INCLUDE_DIR}/clog_dir/qt ${CLOG_INSTALL_PERMISSIONS} )

# clog_print headers
install(FILES clog_dir/print/print.h
    DESTINATION ${INSTALL_INCLUDE_DIR}/clog_dir/print ${CLOG_INSTALL_PERMISSIONS} )
